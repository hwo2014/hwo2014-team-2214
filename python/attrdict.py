class AttrDict(dict):
    """
    >>> o = {'a':1, 'b':2, 'c':'hana', 'd': {'e':100, 'f':{'g':'treasure'}}}
    >>> d = AttrDict(o)
    >>> print(d.a, d.b, d.c, d.d, d.d.e, d.d.f, d.d.f.g)
    (1, 2, 'hana', {'e': 100, 'f': {'g': 'treasure'}}, 100, {'g': 'treasure'}, 'treasure')
    >>> d.author = 'dgoon'
    >>> d.author
    'dgoon'
    >>> d.update({'it':'works?'})
    >>> d.it
    'works?'
    """
    __setattr__ = dict.__setitem__

    def __getattr__(self, k):
        v = self.__getitem__(k)
        return v if not isinstance(v, dict) else AttrDict(v)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
