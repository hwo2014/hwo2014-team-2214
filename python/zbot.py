import json
import socket
import sys
import datetime
import math
from attrdict import AttrDict

def stddev(l):
    l=list(l)
    s = 0.0
    ss = 0.0
    for x in l:
        s += x
        ss += x**2
    avg = s / len(l)
    avg_of_square = ss / len(l)
    return math.sqrt(avg_of_square - avg**2)

def nowstr():
    return datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

class ZBot(object):
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.reset()

    def reset(self):
        self.turbo = {}
        self.prev_pos = []
        self.tick = 0
        self.th = 0.66666

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data, 'gameTick': self.tick}))

    def switch(self, shorter):
        self.send(json.dumps({"msgType": "switchLane", "data": shorter, 'gameTick': self.tick}))

    def gogosing(self):
        self.send(json.dumps({'msgType': 'turbo', 'data': 'GoGoSing!!!', 'gameTick': self.tick}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_start(self, data):
        self.reset()
        print("Race started")
        self.ping()

    def _get_speed(self, cur_pos):
        if not self.prev_pos:
            return 0.0
        prev_me = self.prev_pos[-1]
        prev_pos = prev_me.piecePosition
        if prev_pos.pieceIndex == cur_pos.pieceIndex:
            return (cur_pos.inPieceDistance - prev_pos.inPieceDistance) * 60.0
        else:
            prev_track = self.tracks[prev_pos.pieceIndex]
            prev_lane = prev_pos.lane.endLaneIndex
            prev_lanelength = prev_track.lanelength.get(prev_lane)
            #print('prev_lanelength:%.3f, prev_dist:%.3f' % (prev_lanelength, prev_pos.inPieceDistance))
            dist = (prev_lanelength - prev_pos.inPieceDistance) + cur_pos.inPieceDistance
            return dist * 60.0

    def _shorter_side(self, cur_pos):
        cur_n = cur_pos.pieceIndex
        n = (cur_n+1) % len(self.tracks)
        cur_lane = cur_pos.lane.startLaneIndex
        lanelength = {}
        sw_cnt = 0
        while True:
            next_track = self.tracks[n]
            #print n, next_track
            n = (n+1) % len(self.tracks)

            for k, v in next_track.lanelength.items():
                lanelength.setdefault(k, 0.0)
                lanelength[k]+=v
            if next_track.switch:
                sw_cnt += 1
            if sw_cnt == 2:
                break
        l = lanelength.get(cur_lane-1, 1000000000)
        c = lanelength.get(cur_lane)
        r = lanelength.get(cur_lane+1, 1000000000)
        #print('Current lane: %d, l:%.4f    r:%.4f' % (cur_lane, l, r))
        if l<r and l<c:
            return "Left"
        elif l>r and c>r:
            return "Right"
        else:
            return "Keep"

    def on_car_positions(self, data):
        cars = map(AttrDict, data)

        me = [car for car in cars if car.id.name == self.name][0]

        my_pos = me.piecePosition
        my_pos.pieceIndex
        my_pos.inPieceDistance

        speed = self._get_speed(my_pos)
        track = self.tracks[my_pos.pieceIndex]
        next_track = self.tracks[(my_pos.pieceIndex+1)%len(self.tracks)]
        try:
            if next_track.switch and next_track.switch_check <= my_pos.lap:
                next_track.switch_check += 1
                if abs(me.angle) > 44 and speed > 0.8 * 600:
                    shorter = 'Keep'
                else:
                    shorter = self._shorter_side(my_pos)
                if shorter != 'Keep':
                    print('Switch to %s' % shorter)
                    self.switch(shorter)
                    return
            es = track.expected_speed
            prev_me = self.prev_pos[-1] if self.prev_pos else me
            prev_angle = prev_me.angle
            cur_angle = me.angle
            angle_diff = abs(cur_angle) - abs(prev_angle)
            angle_stddev = stddev(x.angle for x in self.prev_pos) if self.prev_pos else 0.0
            if self.turbo and track.turbo==self.max_turbo:
            #if self.turbo and next_track.turbo==self.max_turbo:# and my_pos.inPieceDistance >= track['lanelength'][0]*0.7:
                print('Turbo!')
                self.gogosing()
                self.turbo = {}
                return
            if speed < es and (angle_diff < 1.5 or abs(me.angle) < 1.0):
                self.th = 1.0
            elif angle_diff > 1.5:
                self.th = 0.0
            #elif speed > es + 10.0:
            #    self.th = 0.0
            elif speed > es:
                self.th = 0.0
                #self.th = 0.5
            elif abs(me.angle) > 0.0 and angle_stddev<1.0:
                self.th = 0.8
            else:
                self.th = 0.0
            # zzugg added
            if track['expected_speed'] < 600 and abs(me.angle) > 50 and speed > 0.7 * 600:
                self.th = 0.0
            if track['expected_speed'] < 600 and abs(me.angle) > 55:
                self.th = 0.0
            #if my_pos.lap == self.laps - 1 and track.last_lap_unlimit:
            #    self.th = 1.0
            print('%04d: throttle: %.2f    speed:%.3f    expected_speed:%.3f    angle:%.2f    pos:%d,%.4f' % (self.tick, self.th, speed, es, me.angle, my_pos.pieceIndex, my_pos.inPieceDistance))
            self.msg("throttle", self.th)
        finally:
            self.prev_pos.append(me)
            self.prev_pos = self.prev_pos[-3:]

    def on_crash(self, data):
        print("Someone crashed")
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_game_init(self, data):
        race = data['race']
        track = race['track']
        cars = race['cars']
        lanes = track['lanes']
        raceSession = race['raceSession']

        self.tracks = []
        for i, p in enumerate(track['pieces']):
            r = p.get('radius', 0.0)
            a = p.get('angle', 0.0)
            p['lanelength'] = {}
            for l in lanes:
                if abs(a)>0.0:
                    sign = 1.0 if a>0.0 else -1.0
                    p['lanelength'][l['index']] = abs((r-sign*l['distanceFromCenter'])*math.pi*a/180.0)
                else:
                    p['lanelength'][l['index']] = p['length']
            p['radius'] = r
            p['angle'] = a
            p['switch'] = p.get('switch')
            p['switch_check'] = 0
            p['expected_speed'] = 0.6 * 600
            p['turbo'] = 0
            p['last_lap_unlimit'] = False
            self.tracks.append(AttrDict(p))
        self._assign_expected_speed()
        #self.laps = raceSession['laps']
        self.ping()

    def _assign_expected_speed(self):
        self.max_turbo = 0
        for n in range(len(self.tracks)):
            ct = self.tracks[n]
            nt = self.tracks[(n+1)%len(self.tracks)]
            nnt = self.tracks[(n+2)%len(self.tracks)]
            try:
                for nn in range(n, n+len(self.tracks)):
                    idx = nn % len(self.tracks)
                    if self.tracks[idx].angle == 0.0:
                        ct.turbo += 1
                    else:
                        break
                if ct.turbo > self.max_turbo:
                    self.max_turbo = ct.turbo

                if ct.turbo > 4:
                    ct.expected_speed = 2.0 * 600
                #elif ct.turbo == 5:
                #    ct.expected_speed = 1.5 * 600
                elif ct.turbo > 1:
                    ct.expected_speed = 1.0 * 600
                elif ct.turbo > 0:
                    ct.expected_speed = 0.77 * 600
                elif nt.angle == nnt.angle == 0.0:
                    ct.expected_speed = 1.0 * 600
                elif nt.angle == 0.0:
                    ct.expected_speed = 0.9 * 600
                elif ct.angle * nt.angle < 0:
                    ct.expected_speed = 1.0 * 600
                else:
                    if ct.angle * nt.angle < 0.0:
                        ct.expected_speed = 1.0 * 600
                    elif ct.radius <= 100.0:
                        ct.expected_speed = 0.675 * 600
                    ct.expected_speed = 0.7 * 600
            finally:
                print('Track #%03d: Turbo %d    Angle %.3f    radius %.3f    expected_speed %.3f    lanelength %s' \
                        % (n, ct.turbo, ct.angle, ct.radius, ct.expected_speed, ' '.join('%.3f(%d)' % (l,i) for i, l in sorted(ct.lanelength.items()))))

        for n in range(len(self.tracks)):
            ct = self.tracks[len(self.tracks)-1-n]
            if ct.turbo > 0:
                ct.last_lap_unlimit = True
            else:
                break

    def on_dnf(self, data):
        print('DNF')
        print data

    def on_turbo(self, data):
        self.turbo = AttrDict({
            'factor': data['turboFactor'],
            'ticks': data['turboDurationTicks'],
        })

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'turboAvailable': self.on_turbo,
            'dnf': self.on_dnf,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            self.tick = max(msg.get('gameTick', -1), self.tick)
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


