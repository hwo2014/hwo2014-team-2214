import sys
import socket
from optparse import OptionParser
from attrdict import AttrDict

from dbot import DBot
from zbot import ZBot

test_server = 'prost.helloworldopen.com'

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option('-s', '--server', dest='server', default=test_server,
            help='race server host ip/address', action='store')
    parser.add_option('-p', '--port', dest='port', default=8091,
            type='int', help='race server port', action='store')
    parser.add_option('-b', '--botname', dest='botname', default='DBot',
            help='your bot\'s name', action='store')
    parser.add_option('-k', '--botkey', dest='botkey', default='PfoYEwU8QTrXaQ',
            help='your bot\'s key', action='store')
    parser.add_option('-t', '--track', dest='track', default='keimola',
            help='choose: keimola germany usa france', action='store')
    parser.add_option('-n', '--cars', dest='cars', default=1,
            help='number of cars in the race', action='store', type='int')
    option, args = parser.parse_args()
    if args:
        print('Unknown arguments: %s' % args)
        sys.exit(0)

    s = socket.socket()
    s.connect((option.server, option.port))
    bot = eval(option.botname)(s, option.botname, option.botkey)
    bot.create_race(option.track, option.cars)
    bot.start_race()
